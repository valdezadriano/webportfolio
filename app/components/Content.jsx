import React from 'react';
import Slideshow from './Slideshow';


const divStyle = {
  transform: 'translateY(0px) translateZ(0px)',
};

const urls = [
  './images/house-2.jpg',
  './images/memory-2.jpg',
  './images/chat-2.jpg',
  './images/maze-2.jpg',
  '',
  './images/page-2.jpg',
  './images/redux-2.jpg',
  './images/portfolio-2.jpg',

];

const albumUrls = [
  './images/house-1.jpg',
  './images/memory-1.jpg',
  './images/chat-1.jpg',
  './images/maze-1.jpg',
  '',
  './images/page-1.jpg',
  './images/redux-1.jpg',
  './images/portfolio-1.jpg',
];


const songTitle = [
  'CSS: position: absolute & position: relative',
  'DOM: Javascript dom access + manipulation',
  'xxxxxxxxxx:Client-Server comunication',
  'React: stack de desarrollo',
  'xxxxxxxxxx:linting and testing',
  'Proyecto: página web',
  'Redux: stack de desarrollo',
  'Proyecto final: portafolio',
];

const ref = [
  'https://codepen.io/anon/pen/xrMvLm',
  'https://codepen.io/anon/pen/QMNQPm?editors=0010',
  '',
  'https://github.com/adriano515/MazeReact',
  '',
  'https://gitlab.com/valdezadriano/webpagereact',
  'https://gitlab.com/valdezadriano/reduxcounter/',
  'https://gitlab.com/valdezadriano/webportfolio',


];

const Content = () =>

  (
    <div className="js-content u-pos-tl u-fit-w u-overflow-h u-bg--white u-fixed" style={divStyle}>
      <Slideshow
        imageUrls={urls}
        albumImageUrls={albumUrls}
        songTitle={songTitle}
        refs={ref}
      />
    </div>
  );

export default Content;

