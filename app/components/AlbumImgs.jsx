import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

let divStyle = {
  backgroundColor: 'rgba(26, 95, 133, 0.01)',
  transform: 'translateX(0px) translateZ(0px)',
};

const infoDivStyle = {
  backgroundColor: 'rgba(26, 95, 133, 0.01)',
};

const trackStyle = {
  color: 'rgb(88, 211, 225)',
};

const lineStyle = {
  background: 'rgb(88, 211, 225) none repeat scroll 0% 0%',
};

const circle1Style = {
  backgroundColor: 'rgb(228, 119, 92)',
};

const circle2Style = {
  backgroundColor: 'rgb(235, 205, 194)',

};
const circle3Style = {
  backgroundColor: 'rgb(31, 102, 117)',

};

class AlbumImgs extends React.Component {
  render() {
    const keys = this.props.keys + 1;
    const classes = classNames('c-track-item u-w4of12 u-force-3d u-inline-block\n' +
        '      u-white-space-normal | u-w5of12@lg u-w10of12@md u-w5of6@sm js-elem\n' +
        '      c-track-item--0', {
      'is-active': this.props.isHovered,
    });
    if (!this.props.isHovered && this.props.isHover !== 0) {
      if (this.props.isHover > keys) {
        divStyle = {
          backgroundColor: 'rgba(26, 95, 133, 0.01)',
          transform: 'translateX(-160px) translateZ(0px)',
        };
      } else {
        divStyle = {
          backgroundColor: 'rgba(26, 95, 133, 0.01)',
          transform: 'translateX(160px) translateZ(0px)',
        };
      }
    } else {
      divStyle = {
        backgroundColor: 'rgba(26, 95, 133, 0.01)',
        transform: 'translateX(0px) translateZ(0px)',
      };
    }
    return (
      <div
        className={classes}
        style={divStyle}
        onMouseOver={() => this.props.hoverHandler(this.props.keys)}
        onFocus={() => this.props.hoverHandler(this.props.keys)}
        onMouseOut={() => this.props.mouseOutHandler(this.props.keys)}
        onBlur={() => this.props.mouseOutHandler(this.props.keys)}
      >
        <a
          href={this.props.refs}
          className="c-track-item-inner
        u-relative u-block"
          draggable="false"
        >
          <span className="c-track-item-number t-text u-bold
          u-absolute u-offset-l-w1of8"
          >0{keys}
          </span>
          <div className="c-track-item-cover u-relative u-w5of8 u-offset-l-w3of8 u-pad-x-md">
            <div
              r="-10,10,0"
              className="js-parent
            c-track-item-cover-inner u-fit u-block u-relative"
            >
              <div className="u-absolute u-pos-tl u-fit u-force-3d" />
              <div className="c-track-item-cover-img
              u-absolute u-pos-tl u-fit  u-force-3d u-overflow-h"
              >
                <img
                  src={this.props.albumImageUrl}
                  draggable="false"
                  className="u-absolute u-pos-tl u-fit u-force-3d"
                  alt="album"
                />
              </div>
            </div>
          </div>
          <div
            className="u-absolute u-fit-h u-pos-tl u-flex u-align-items-c
           u-w3of5 u-pointer-none u-force-3d"
            style={infoDivStyle}
          >
            <div>
              <h2 className="c-track-item-author t-h2 u-color--brand-alt">
                {this.props.songTitle}
              </h2>
              <h3
                className="c-track-item-title t-text--xl u-medium u-marg-t-xs"
                style={trackStyle}
              >
                {this.props.songTitle}
              </h3>
              <span className="c-track-item-line c-line u-inline-block">
                <span
                  className="c-line-1"
                  style={lineStyle}
                />
                <span
                  className="c-line-2"
                  style={lineStyle}
                />
                <span
                  className="c-line-3"
                  style={lineStyle}
                />
              </span>
              <div className="u-marg-t-xs u-flex">
                <span
                  className="c-track-item-color"
                  style={circle1Style}
                />
                <span
                  className="c-track-item-color"
                  style={circle2Style}
                />
                <span
                  className="c-track-item-color"
                  style={circle3Style}
                />
              </div>
            </div>
          </div>
        </a>
      </div>
    );
  }
}


AlbumImgs.propTypes = {
  albumImageUrl: PropTypes.string.isRequired,
  keys: PropTypes.number.isRequired,
  songTitle: PropTypes.string.isRequired,
  isHovered: PropTypes.bool.isRequired,
  isHover: PropTypes.number.isRequired,
  hoverHandler: PropTypes.func.isRequired,
  mouseOutHandler: PropTypes.func.isRequired,
  refs: PropTypes.string.isRequired,
};

export default AlbumImgs;

